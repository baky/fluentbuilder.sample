using System;

namespace FluentBuilder.Sample.Builders
{
    public abstract class ChildOrderBuilderBase : IOrderBuilder
    {
        private readonly IOrderBuilder _parentBuilder;

        public Order Order
        {
            get { return _parentBuilder.Order; }
        }

        public OrderBuilder At(DateTime dateTime)
        {
            return _parentBuilder.At(dateTime);
        }

        public OrderCustomerBuilder For()
        {
            return _parentBuilder.For();
        }

        public OrderProductBuilder WithProduct()
        {
            return _parentBuilder.WithProduct();
        }

        protected ChildOrderBuilderBase(IOrderBuilder parentBuilder)
        {
            _parentBuilder = parentBuilder;
        }

        public static implicit operator Order(ChildOrderBuilderBase builder)
        {
            return builder.Order;
        }
    }
}