namespace FluentBuilder.Sample.Builders
{
    public abstract class OrderProductBuilder : ChildOrderBuilderBase, IProductBuilder
    {
        protected OrderProductBuilder(IOrderBuilder parentBuilder)
            : base(parentBuilder)
        {
        }

        public abstract OrderProductBuilder InAmount(int quantity);
        public abstract OrderProductBuilder WithName(string name);
    }
}