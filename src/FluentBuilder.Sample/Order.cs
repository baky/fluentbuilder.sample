﻿using System;
using System.Collections.Generic;

namespace FluentBuilder.Sample
{
    public class Order
    {
        public DateTime CreatedAt { get; set; }
        public Customer Customer { get; set; }
        public IList<Product> Products { get; set; }
    }
}
