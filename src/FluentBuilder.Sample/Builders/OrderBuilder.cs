using System;
using System.Collections.Generic;

namespace FluentBuilder.Sample.Builders
{
    public class OrderBuilder : IOrderBuilder
    {
        public Order Order { get; private set; }

        public OrderBuilder(Order order)
        {
            Order = order;
            Order.Products = new List<Product>();
        }

        public OrderBuilder At(DateTime dateTime)
        {
            Order.CreatedAt = dateTime;
            return this;
        }

        public OrderCustomerBuilder For()
        {
            Order.Customer = new Customer();
            return new CustomerBuilder(this, Order.Customer);
        }

        public OrderProductBuilder WithProduct()
        {
            var product = new Product();
            Order.Products.Add(product);
            return new ProductBuilder(this, product);
        }

        public static implicit operator Order(OrderBuilder builder)
        {
            return builder.Order;
        }
    }
}