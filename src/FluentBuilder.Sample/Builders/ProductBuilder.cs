namespace FluentBuilder.Sample.Builders
{
    public class ProductBuilder : OrderProductBuilder
    {
        private readonly Product _product;

        public ProductBuilder(IOrderBuilder parentBuilder, Product product) : base(parentBuilder)
        {
            _product = product;
        }

        public override OrderProductBuilder InAmount(int quantity)
        {
            _product.Quantity = quantity;
            return this;
        }

        public override OrderProductBuilder WithName(string name)
        {
            _product.Name = name;
            return this;
        }
    }
}