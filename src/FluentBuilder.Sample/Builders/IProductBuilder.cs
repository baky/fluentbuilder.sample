namespace FluentBuilder.Sample.Builders
{
    public interface IProductBuilder
    {
        OrderProductBuilder InAmount(int quantity);
        OrderProductBuilder WithName(string name);
    }
}