using System;

namespace FluentBuilder.Sample.Builders
{
    public interface IOrderBuilder
    {
        Order Order { get; }
        OrderBuilder At(DateTime dateTime);
        OrderCustomerBuilder For();
        OrderProductBuilder WithProduct();
    }
}