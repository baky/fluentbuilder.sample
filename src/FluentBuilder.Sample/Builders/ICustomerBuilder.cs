namespace FluentBuilder.Sample.Builders
{
    public interface ICustomerBuilder
    {
        OrderCustomerBuilder WithFullName(string fullName);
    }
}