namespace FluentBuilder.Sample.Builders
{
    public class CustomerBuilder : OrderCustomerBuilder
    {
        private readonly Customer _customer;

        public CustomerBuilder(IOrderBuilder parentBuilder, Customer customer)
            : base(parentBuilder)
        {
            _customer = customer;
        }

        public override OrderCustomerBuilder WithFullName(string fullName)
        {
            _customer.FullName = fullName;
            return this;
        }
    }
}