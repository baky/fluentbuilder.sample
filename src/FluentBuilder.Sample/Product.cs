namespace FluentBuilder.Sample
{
    public class Product
    {
        public int Quantity { get; set; }
        public string Name { get; set; }
    }
}