namespace FluentBuilder.Sample.Builders
{
    public abstract class OrderCustomerBuilder : ChildOrderBuilderBase, ICustomerBuilder
    {
        protected OrderCustomerBuilder(IOrderBuilder parentBuilder)
            : base(parentBuilder)
        {
        }

        public abstract OrderCustomerBuilder WithFullName(string fullName);
    }
}