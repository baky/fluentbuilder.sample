﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentBuilder.Sample.Builders;

namespace FluentBuilder.Sample.Tests
{
    [TestClass]
    public class OrderFactoryTest
    {
        [TestMethod]
        public void MainTest()
        {
            Order order = OrderFactory
                .Create()
                    .For()
                        .WithFullName("John Dow")
                    .WithProduct()
                        .WithName("First Product")
                        .InAmount(1)
                    .WithProduct()
                        .WithName("Second Product")
                        .InAmount(2)
                    .At(DateTime.Now);

            Assert.IsNotNull(order);
            Assert.IsNotNull(order.Customer);
            Assert.IsNotNull(order.Products);
            Assert.AreEqual(order.Products.Count, 2);
        }
    }
}
