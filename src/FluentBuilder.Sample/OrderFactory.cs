using FluentBuilder.Sample.Builders;

namespace FluentBuilder.Sample
{
    public class OrderFactory
    {
        public static OrderBuilder Create()
        {
            return new OrderBuilder(new Order());
        }
    }
}